package exceptions;
/**
 * @author mathiasrudig
 * @version 1.0
 */
public class InvalidFloorException extends Exception {
    public InvalidFloorException(String message) {
        super(message);
    }
    public InvalidFloorException() {
        super("Ungueltiges Stockwerk");
    }
}
