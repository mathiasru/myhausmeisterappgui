package infrastruktur;

import java.util.Scanner;

/**
 * Die Klasse implementiert Methoden, welche Eingaben auf deren Gültigkeit prüfen.
 * @author mathiasrudig
 * @version 1.0
 */
public class InputChecker {
    /**
     * Die Methode ermittelt per Eingabe eine Ganzzahl in einem gültigen Bereich und liefert diese zurück.
     * @param start übergebener Startpunkt des Gültigkeitbereichs vom Typ int.
     * @param ende übergebener Endpunkt des Gültigkeitbereichs vom Typ int.
     * @return Liefert eine Ganzzahl in einem gültigen Bereich zurück.
     */
    public static int getWertInnerhalbGueltigemBereich(int start, int ende) {
        Scanner scan = new Scanner(System.in);
        int eingabe = 0;
        boolean fehlerhafteEingabe = true;
        do {
            try {
                System.out.println("Bitte geben Sie eine Zahl zwischen " + start + " und " + ende + " ein");
                System.out.print("> ");
                eingabe = Integer.parseInt(scan.nextLine());
                if (eingabe >= start && eingabe <= ende) {
                    fehlerhafteEingabe = false;
                }
            } catch (Exception exception) {
                fehlerhafteEingabe = true;
            }
        } while (fehlerhafteEingabe);
        return eingabe;
    }

    /**
     * Die Methode ermittelt per Eingabe einen String mit einer gültigen Länge und liefert diesen zurück.
     * @param anzahlZeichen übergebene Länge des Strings vom Typ int.
     * @return Liefert einen String mit einer gültigen Länge zurück.
     */
    public static String getStringMitGueltigerLaenge(int anzahlZeichen) {
        Scanner scan = new Scanner(System.in);
        String eingabe = "";
        boolean fehlerhafteEingabe = true;
        do {
                System.out.print("> ");
                eingabe = scan.nextLine();
                if (eingabe.length() <= anzahlZeichen) {
                    fehlerhafteEingabe = false;
                } else {
                    System.out.println("Maximale Anzahl an Zeichen überschritten");
                    fehlerhafteEingabe = true;
                }
        } while (fehlerhafteEingabe);
        return eingabe;
    }

}
