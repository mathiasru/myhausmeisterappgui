package domain.buildingstructure;

import domain.interference.SammlungStoerungen;
import exceptions.InvalidFloorException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Die Klasse implementiert eine Sammlung von Stockwerken, welche über Methoden erweitert respektive um Einträge
 * verkleinert werden kann.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public class StockwerkListe implements Serializable {
    //Datenfelder
    private List<Stockwerk> stockwerkListe;

    /**
     * Konstruktor erzeugt eine neue Sammlung von Stockwerken in Form einer ArrayList.
     */
    public StockwerkListe() {
        this.stockwerkListe = new ArrayList<>();
    }

    /**
     * Die Methode implementiert das Hinzufügen von Stockwerken, welche auf Gültigkeit(darf nur einmal vorkommen)
     * geprüft werden, ansonsten wird eine Exception(InvalidFloorException) ausgelöst.
     *
     * @param stockwerk übergebene Stockwerk vom Typ Stockwerk.
     * @throws InvalidFloorException beim Übergeben von ungültigen Stockwerken vom Typ Stockwerk.
     */
    public void hinzufuegen(Stockwerk stockwerk) throws InvalidFloorException {
        if (stockwerk != null && !stockwerkListe.contains(stockwerk)) {
            stockwerkListe.add(stockwerk);
        } else {
            throw new InvalidFloorException();
        }
    }

    /**
     * Die Methode implementiert das Entfernen von Stockwerken, welche in der bestehenden Liste vorhanden sein müssen,
     * ansonsten wird eine Exception(InvalidFloorException) ausgelöst.
     *
     * @param stockwerk übergebene Stockwerk vom Typ Stockwerk.
     * @throws InvalidFloorException beim Übergeben von ungültigen Stockwerken vom Typ Stockwerk.
     */
    public void entfernen(Stockwerk stockwerk) throws InvalidFloorException {
        if (stockwerk != null && stockwerkListe.contains(stockwerk)) {
            stockwerkListe.remove(stockwerk);
        } else {
            throw new InvalidFloorException();
        }
    }

    /**
     * Die Methode implementiert eine Ausgabe der gesamten Liste aller Räume.
     */
    public void ausgeben() {
        int index = 1;
        for (Stockwerk stockwerke : stockwerkListe) {
            if (stockwerke != null) {
                System.out.println(index + " " + stockwerke);
            }
            index++;
        }
    }

    /**
     * Die Methode liefert eine Liste von Stockwerken zurück.
     *
     * @return Liefert eine Liste von Stockwerken vom Typ Stockwerk zurück.
     */
    public List<Stockwerk> getStockwerkListe() {
        return stockwerkListe;
    }

}
