package domain.buildingstructure;

import exceptions.InvalidRoomException;
import domain.interference.SammlungStoerungen;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Die Klasse implementiert eine Sammlung von Räumen, welche über Methoden erweitert respektive um Einträge
 * verkleinert werden kann.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public class RaumListe implements Serializable {
    //Datenfelder
    private List<Raum> raumListe;

    /**
     * Konstruktor erzeugt eine neue Sammlung von Räumen in Form einer ArrayList.
     */
    public RaumListe() {
        this.raumListe = new ArrayList<>();
    }

    /**
     * Die Methode implementiert das Hinzufügen von Räumen, welche auf Gültigkeit(darf nur einmal vorkommen)
     * geprüft werden, ansonsten wird eine Exception(InvalidRoomException) ausgelöst.
     *
     * @param raum übergebene Raum vom Typ Raum.
     * @throws InvalidRoomException beim Übergeben von ungültigen Räumen vom Typ Raum.
     */
    public void hinzufuegen(Raum raum) throws InvalidRoomException {
        if (raum != null && !raumListe.contains(raum)) {
            raumListe.add(raum);
        } else {
            throw new InvalidRoomException();
        }
    }

    /**
     * Die Methode implementiert das Entfernen von Räumen, welche in der bestehenden Liste vorhanden sein müssen,
     * ansonsten wird eine Exception(InvalidRoomException) ausgelöst.
     *
     * @param raum übergebene Raum vom Typ Raum.
     * @throws InvalidRoomException beim Übergeben von ungültigen Räumen vom Typ Raum.
     */
    public void entfernen(Raum raum) throws InvalidRoomException {
        if (raum != null && raumListe.contains(raum)) {
            raumListe.remove(raum);
        } else {
            throw new InvalidRoomException();
        }
    }

    /**
     * Die Methode implementiert eine Ausgabe der gesamten Liste aller Räume.
     */
    public void ausgeben() {
        int index = 1;
        for (Raum raeume : raumListe) {
            if (raeume != null) {
                System.out.println( index + " " + raeume);
            }
            index++;
        }
    }

    /**
     * Die Methode liefert eine Liste von Räumen zurück.
     *
     * @return Liefert eine Liste von Räumen vom Typ Raum zurück.
     */
    public List<Raum> getRaumListe() {
        return raumListe;
    }

}
