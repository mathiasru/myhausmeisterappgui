package domain.interference;

import exceptions.InvalidStoerungException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Die Klasse implementiert eine Sammlung von Störungen, welche über Methoden erweitert respektive um Einträge
 * verkleinert werden kann. Zudem stellt sie mehrere Methoden zum Filtern der Ausgaben bereit.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public class SammlungStoerungen implements Serializable {
    //Datenfelder
    private ArrayList<Stoerung> stoerungListe;

    /**
     * Konstruktor erzeugt eine neue Sammlung von Störungen in Form einer ArrayList.
     */
    public SammlungStoerungen() {
        this.stoerungListe = new ArrayList<>();
    }

    /**
     * Die Methode implementiert das Hinzufügen von Störungen, welche auf Gültigkeit(darf nur einmal vorkommen)
     * geprüft werden, ansonsten wird eine Exception(InvalidStoerungException) ausgelöst.
     *
     * @param stoerung übergebene Störung vom Typ Störung.
     * @throws InvalidStoerungException beim Übergeben von ungültigen Störungen vom Typ Stoerung.
     */
    public void stoerungHinzufuegen(Stoerung stoerung) throws InvalidStoerungException {
        if (stoerung != null && !this.stoerungListe.contains(stoerung)) {
            this.stoerungListe.add(stoerung);
        } else {
            throw new InvalidStoerungException();
        }
    }

    /**
     * Die Methode implementiert das Entfernen von Störungen, welche in der bestehenden Liste vorhanden sein müssen,
     * ansonsten wird eine Exception(InvalidStoerungException) ausgelöst.
     *
     * @param stoerung übergebene Störung vom Typ Störung.
     * @throws InvalidStoerungException beim Übergeben von ungültigen Störungen vom Typ Stoerung.
     */
    public void stoerungEntfernen(Stoerung stoerung) throws InvalidStoerungException {
        if (stoerung != null && this.stoerungListe.contains(stoerung)) {
            this.stoerungListe.remove(stoerung);
        } else {
            throw new InvalidStoerungException();
        }
    }

    /**
     * Die Methode implementiert eine Ausgabe der gesamten Liste aller Störungen.
     */
    public void stoerungsListeGesamtAusgeben() {
        int index = 1;
        for (Stoerung stoerung : this.stoerungListe) {
            if (stoerung != null) {
                System.out.println(index + " " + stoerung);
            }
            index++; //steht außerhalb, damit im falle von null-Objekten trotzem itteriert wird
        }
    }

    /**
     * Die Methode implementiert eine gefilterte Ausgabe nach einem bestimmten Status der Störung.
     *
     * @param statusStoerung übergebener Status der Störung vom Typ StatusStoerung.
     */
    public void stoerungsListeNachStatusAusgeben(StatusStoerung statusStoerung) {
        int index = 1;
        for (Stoerung stoerung : this.stoerungListe) {
            if (stoerung != null && stoerung.getStatus().getStatusStoerungBeschreibung().equals(statusStoerung.getStatusStoerungBeschreibung())) {
                System.out.println(index + " " + stoerung);
                index++;
            }
        }
    }

    /**
     * Die Methode implementiert eine gefilterte Ausgabe nach einem bestimmten Gewerk der Störung.
     *
     * @param gewaerk übergebenes Gewerk vom Typ Gewerke.
     */
    public void stoerungsListeNachGewerkAusgeben(Gewerke gewaerk) {
        int index = 1;
        for (Stoerung stoerung : this.stoerungListe) {
            if (stoerung != null && stoerung.getGewaerk().getunGewerkeBeschreibung().equals(gewaerk.getunGewerkeBeschreibung())) {
                System.out.println(index + " " + stoerung);
                index++;
            }
        }
    }

    /**
     * Die Methode implementiert eine gefilterte Ausgabe und gibt nur jene Störungen aus, die eine
     * Deadline haben.
     */
    public void stoerungsListeNachDeadlineAusgeben() {
        int index = 1;
        for (Stoerung stoerung : this.stoerungListe) {
            if (stoerung != null && stoerung instanceof StoerungMitDeadline) {
                System.out.println(index + " " + stoerung);
                index++;
            }
        }
    }

    /**
     * Die Methode liefert eine Liste von Störungen zurück.
     *
     * @return Liefert eine Liste von Störungen vom Typ Störung zurück.
     */
    public List<Stoerung> getSammlungStoerung() {
        return this.stoerungListe;
    }

}
